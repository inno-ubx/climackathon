# Climackathon

**Un "hackathon" pour mieux communiquer sur le changement climatique à l'université de Bordeaux et faire changer les comportements**

Comment engager le public sur le changement climatique et se motiver à changer nos comportements ? Comment dépasser la culpabilisation pour créer de nouvelles normes sociales pro-environnementales ? Comment utiliser des astuces psychologiques pour influencer nos actions ? Dans cet **atelier créatif et collaboratif**, nous nous appuierons sur les savoirs issus de la psychologie sociale, du marketing et des neurosciences **pour imaginer des outils de communication et de sensibilisation plus efficaces**. 

Un premier temps sera consacré à un éclairage scientifique sur ce qui fonctionne, ce qui ne fonctionne pas, et les explications biologiques ou sociales à nos comportements. Puis un atelier en petits groupes permettra d'appliquer ces méthodes à trois sujets :
- en matière de **mobilité**, nous réfléchirons aux meilleures manières d'influencer nos choix plus ou moins éco-responsables, en nous appuyant sur une application mobile en cours de développement avec La Rochelle Université
- en ce qui concerne **la production et la consommation**, nous réfléchirons à l'empreinte carbone de ce qui est acheté et produit par l'université, et aux manières d'encourager la substitution
- pour les **éco-gestes du quotidien**, nous reprendrons la [campagne de sensibilisation lancée en 2016 à l'université](https://www.u-bordeaux.fr/Actualites/De-l-universite/Un-mois-un-eco-geste) pour l'améliorer.

**Les outils et solutions issus de l'atelier seront, dans la mesure du possible, mis en œuvre à l'université de Bordeaux**.

**Toutes les communautés du campus sont invitées à participer**, en particulier celles et ceux qui s'intéressent aux questions de communication et d'engagement. Aucune connaissance préalable n'est nécessaire, mais vous êtes encouragés à [consulter quelques ressources sélectionnées](https://ulab.u-bordeaux.fr/ulabtest/index.php?r=content%2Fperma&id=4010) pour vous préparer. Venez réfléchir, venez apprendre, et surtout venez agir pour mieux communiquer sur le changement climatique et faire changer les comportements. Le nombre de places étant limité, nous nous réservons la possibilité de sélectionner les participant.e.s pour une meilleure représentativité.

Chercheurs :
- Marie-Line Félonneau (Labpsy)
- Alexandre Pascual - sous réserve (Labpsy)
- Hélène Labarre (Labpsy)
- Juliette Passebois-Ducros (IRGO)
- Frédéric Alexandre (Inria / IMN)
- Nicolas Rougier (Inria / IMN)

Facilitateurs :
- Antoine Blanchard, Service innovation / Bureau impulsion (RIPI/DIPE) - antoine.blanchard@u-bordeaux.fr
- Hélène Schwalm, Service innovation / Bureau impulsion (RIPI/DIPE) - helene.schwalm@u-bordeaux.fr